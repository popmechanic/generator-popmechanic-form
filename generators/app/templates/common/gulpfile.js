var fileinclude = require('gulp-file-include'),
    gulp = require('gulp'),
    sass = require('gulp-sass'),
    webserver = require('gulp-webserver'),
    imagemin = require('gulp-imagemin');

gulp.task('sass', function () {
    return gulp.src('./*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./build'));
});

gulp.task('fileinclude', function() {
    gulp.src(['index.html'])
        .pipe(fileinclude({
            prefix: '@@',
            basepath: '@file'
        }))
        .pipe(gulp.dest('./build'));
});

gulp.task('img', function() {
    gulp
        .src('./img/*')
        .pipe(imagemin())
        .pipe(gulp.dest('./build/img'));
});

gulp.task('fonts', function() {
    gulp.src('./fonts/*').pipe(gulp.dest('./build/fonts'));
});

gulp.task('sass:watch', function() {
    gulp.watch('./*.scss', ['sass']);
});
gulp.task('html:watch', function() {
    gulp.watch('./*.html', ['fileinclude'])
});

gulp.task('webserver', ['build'], function() {
    gulp.src('build')
        .pipe(webserver({
            open: true
        }));
});

gulp.task('build', ['sass', 'fileinclude', 'img', 'fonts']);

gulp.task('default', ['webserver', 'sass:watch', 'html:watch']);
