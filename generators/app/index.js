'use strict';

var yeoman = require('yeoman-generator');
var chalk = require('chalk');
var yosay = require('yosay');


module.exports = yeoman.generators.Base.extend({
    init: function () {
        var done = this.async();

        this.prompt({
            type: 'list',
            name: 'type',
            message: 'Your form type',
            choices: [{name: 'Multiscreen', value: 'multiscreen'}, {name: 'Simple', value: 'simple'}],
            default: 'simple'
        }, function (answers) {
            this.settings = answers;
            done();
        }.bind(this));
    },

    writeFiles: function () {
        this.fs.copy(
            this.templatePath('common/*'),
            this.destinationPath('./')
        );
        this.fs.copy(
            this.templatePath(this.settings.type + '/*'),
            this.destinationPath('./')
        );
    },

    installDeps: function() {
        this.npmInstall([
            'gulp', 'gulp-sass',
            'gulp-file-include', 'gulp-webserver',
            'gulp-imagemin'
        ], {saveDev: true});
    }

});
