# generator-popmechanic-form
> Yeoman-генератор форм для PopMechanic

## Установка и начало работы

Для начала - установите [Yeoman](http://yeoman.io) и generator-popmechanic-form в [npm](https://www.npmjs.com/).

```bash
npm install -g yo
npm install -g generator-popmechanic-form
```

Потом зайдите в нужную директорию и сгенерируйте проект:

```bash
cd my-new-form
yo popmechanic-form
```

## Что дальше?

Посмотрите файлы формы (```form.html``` и ```style.scss```), запустите формы в браузере: 

```bash
gulp
```

При необходимости - добавьте изображения в папку ```img```, шрифты в папку ```fonts```. Для добавления в кабинет -
перешлите архив с папкой ```build``` нам в PopMechanic.

## Лицензия

MIT © [Глеб Ковалев, PopMechanic](https://popmechanic.ru)
